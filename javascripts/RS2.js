$(window).on("resize", function(){
	console.info("resize triggered");
	var $this = this,
		current_width = parseInt($this.outerWidth, 10);
	RS2.resizedWindow(current_width);
});

var RS2 = (function(){
	function _release_public(){
		return {
			init: init,
			resizedWindow: resizedWindow
		}
	};
	
	/*----------private-----------*/
	

	
	


	/*----------public------------*/

	
	function init(){
		console.log("RS2::init: Good to go!");	
		//console.log("Trigger a the correct resize event on load");
	};
	
	
	/**
	 * Resized Window
	 * 
	 * The window was resized so change the layout
	 * 
	 * @param String current_width
	 *
	 * @return null
	 */
	function resizedWindow(width){
		var size = "out_of_bounds";
		//console.log("RS2::resizedWindow");
		if(width <= 800){
			size = "large";
		}
		if(width <= 600){
			size = "small";
		}
		if(width <= 400){
			size = "very_small";
		}
		console.info("type: " + typeof(width));
		$("body").removeClass().addClass(size);		
		console.log("width: " + width + " to " + size);
		return null;
	};
	
	return _release_public();
})();



	


